define(['i18n!locale/nls/contactUs'], function(i18n) {
  var contactUsTpl ='<div class="modal-contact fade-out" id="contactUsModal">'+
  '<div class="modal-contact-dialog">'+
    '<div class="contact-content">'+
      '<div class="header">'+
        '<span class="title">' + i18n.title + '</span>'+
        '<div class="contact-close">'+
          '<span aria-hidden="true">'+
            '<i class="k-iconfont icon-guanbi"></i>'+
          '</span>'+
        '</div>'+
      '</div>'+

      '<div class="body">'+
        '<div class="item">'+
          '<p class="title">' + i18n.item1.title + '</p>'+
          '<p class="sub-title">' + i18n.item1.subTitle + '</p>'+
          '<a href="' + i18n.item1.btn1Link + '" data-spm-click="gostr=/alicloud;locaid=dcontactus1;" target="_blank" class="btn btn-contact btn-chat">' + i18n.item1.btn1Text + '</a>'+
          '<a href="' + i18n.item1.btn2Link + '" data-spm-click="gostr=/alicloud;locaid=dcontactus2;" target="_blank" class="btn btn-contact">' + i18n.item1.btn2Text + '</a>'+
        '</div>'+

        '<div class="item">'+
          '<p class="title">' + i18n.item2.title + '</p>'+
          '<p class="sub-title">' + i18n.item2.subTitle + '</p>'+
          '<a href="' + i18n.item2.btn1Link + '" data-spm-click="gostr=/alicloud;locaid=dcontactus3;" target="_blank" class="btn btn-contact">' + i18n.item2.btn1Text + '</a>'+
          '</div>'+

        '<div class="item">'+
          '<p class="title">' + i18n.item3.title + '</p>'+
          '<p class="sub-title">' + i18n.item3.subTitle + '</p>'+
          '<a href="' + i18n.item3.btn1Link  + '" data-spm-click="gostr=/alicloud;locaid=dcontactus4;" target="_blank" class="btn btn-contact">' + i18n.item3.btn1Text + '</a>'+
        '</div>'+

      '</div>'+
    '</div>'+
  '</div>'+
'</div>';

	return contactUsTpl
})